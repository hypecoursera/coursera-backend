package data;

import akka.NotUsed;
import akka.stream.javadsl.Source;
import data.models.User;
import domain.exceptions.UserNotFoundException;
import io.ebean.Ebean;
import io.ebean.EbeanServer;
import org.jetbrains.annotations.NotNull;
import play.db.ebean.EbeanConfig;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserRepository {

    private final EbeanServer ebeanServer;

    @Inject
    public UserRepository(EbeanConfig ebeanConfig) {
        this.ebeanServer = Ebean.getServer(ebeanConfig.defaultServer());
    }

    public Source<Boolean, NotUsed> createNewUser(@NotNull User newUser) {
        return Source.fromPublisher(s -> {
            ebeanServer.insert(newUser);
            s.onNext(true);
            s.onComplete();
        });
    }

    public Source<User, NotUsed> getUserByLogin(@NotNull String login) {
        return Source.fromPublisher(s -> {
            User user = Ebean.find(User.class)
                    .where().eq(User.COLUMN_LOGIN, login)
                    .findOne();
            if (user == null) {
                s.onError(new UserNotFoundException());
            } else {
                s.onNext(user);
                s.onComplete();
            }
        });
    }

}
