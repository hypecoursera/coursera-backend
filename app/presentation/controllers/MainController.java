package presentation.controllers;

import play.mvc.Controller;
import play.mvc.Result;

public class MainController extends Controller {

    public Result main() {
        return ok(presentation.views.html.main.render("Hello world"));
    }

}
