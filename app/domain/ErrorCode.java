package domain;

public class ErrorCode {

    static final int UNKNOWN = -1;
    static final int OK = 0;

    //    /user/create
    static final int DUPLICATE_LOGIN = 1;
    static final int DUPLICATE_EMAIL = 2;

    //    /user/auth
    public static final int USER_NOT_FOUND = 3;

}